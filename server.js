// 需求1：编写 Web 服务，支持跨域，前端 GET 请求方式，请求 /api/rand 接口
// 需求2：使用 npm 下载 lodash 使用里面封装的函数，随机产生一个 0 - 10 之间的任意整数:
// 需求3：自定义模块 stuName.js 导出一个数组，值为 10 个名字，并随机取出一个名字返回给前端

//使用 express 搭建web服务
//使用 cors 实现跨域

//导包
const express = require('express')
//引入cors实现跨域
const cors = require('cors')
const _ = require('lodash')
const stuName = require('./stuName')
console.log('stuName')

//创建服务器对象
const server = express()
//安装cors插件支持跨域
server.use(cors())

//监听 get 请求地址
server.get('/api/rand', (req, res) => {
    //生成随机数
    const random = _.random(9)
    // console.log(random)
    res.send(stuName[random])
})

//开启服务器
server.listen(3000, () => {
console.log('服务器已启动')
})



